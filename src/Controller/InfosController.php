<?php

namespace Drupal\site_infos\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class InfosController.
 */
class InfosController extends ControllerBase {

  /**
   * Infos.
   *
   * @return string
   *   Return Hello string.
   */
  public function infos() {
    $files = $this->filesSize();
    return [
      '#type' => 'markup',
      '#markup' => $this->t(
        'Total Files: ' . $files['Total Files'] . ' ' .
        'Total file size: ' . $files['Total File Size']
      ),
    ];
  }

  /**
   *
   */
  private function filesSize() {

    $files = file_scan_directory(file_default_scheme() . '://', "/.*/");
    $filesize = 0;
    $filecount = 0;
    $excludes = ['.php', '.js', '.css', '.po', '.gz'];
    foreach ($files as $file) {
      $ext = substr($file->filename, strrpos($file->filename, '.'));
      if (!in_array($ext, $excludes)) {
        // TODO: this line: public://$file->filename do not work for subdirectories.
        $absolute_path = \Drupal::service('file_system')->realpath("public://$file->filename");
        $filesize += filesize($absolute_path);
        $filecount++;
      }
    }
    $data['Total Files'] = $filecount;
    $data['Total File Size'] = round($filesize / 1024 / 1024, 2) . " MB";

    return $data;
  }

}
